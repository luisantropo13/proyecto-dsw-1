<?php 
        require_once_absolute("views/partials/partial_head.php");
?> 
<body>
    <?php 
        require_once_absolute("views/partials/partial_aside.php");
	    if( isset($_SESSION["user"]) )
	    echo '<meta http-equiv="refresh" content="0; url=http://192.168.56.101/php_user/DSW_Poryecto1/index.php" />';
?>

    <div id="colorlib-main">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="images/imglog.png" alt="IMG">
				</div>

				<form class="login100-form validate-form" method="POST">
					<span class="login100-form-title">
						Entrada de Miembros
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input value="zxcv@gmail.com" class="input100" type="text" name="email" placeholder="Correo">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input value="9874" class="input100" type="password" name="pass" placeholder="Contraseña">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Entrar
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
							Olvidaste el
						</span>
						<a class="txt2" href="#">
							Usuario o la Contraseña?
						</a>
					</div>

					<div class="text-center p-t-136">
						<a class="txt2" href="#">
							 Crea una cuenta
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	<?php 
		require_once_absolute("views/partials/partial_loader.php");
	?>
</body>