    <?php 
        //Una función que hace requerir de la ruta absoluta para llamar al partial_head
        require_once_absolute("views/partials/partial_head.php");
    ?> 
<body>
    <?php 
        //Una función que hace requerir de la ruta absoluta para llamar al partial_aside
        require_once_absolute("views/partials/partial_aside.php");
    ?>

<div id="colorlib-main">
    <div id="colorlib-main">
            <div class="col-xs-12 col-sm-8 col-sm-push-2">
                <h1>GALERÍA</h1>
                <hr>
                <!-- LO DE ABAJO-->
                <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') : ?>
                    <!-- De haber un error a la hora de subir un archivo saltará una alerta con el error -->
                    <div class="alert alert-<?= empty($errores) ? 'info' : 'danger'; ?> alert-dismissible" role="alert">
                        <button type="button" class="clase" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">x</span>
                        </button>
                        <!-- Un condicional que comprueba si no ha habido errores -->
                        <?php if(empty($errores)) : ?>
                        <!-- Si no hay ningun error manda un mensaje de confirmación de subida de archivos-->
                        <p><?=$_SERVER["mensaje"] ?? "" ?></p>
                        <!-- Si hay algun error ... -->
                        <?php else : ?>
                            <ul>
                                <?php foreach($errores as $error) : ?>
                                    <li><?= $error ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>

                <form class="form-horizontal" action="<?=$_SERVER["PHP_SELF"] ?>" method="POST"
                    enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="label-control">Imagen</label>
                            <input class="form-control-file" name="imagen" type="file">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="label-control">Categoria</label>
                            <select name="categoria">
                                <option value="1">Fotos 1 </option>
                                <option value="2">Fotos 2 </option>
                                <option value="3">Fotos 3 </option>
                            </select>                       
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label class="label-control">Descripción</label>
                            <textarea class="form-control" name="descripcion"></textarea>
                            <button class="pull-right btn btn-lg sr-button" style="color:white;">ENVIAR</button>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <table class="table" style="color:white;">
                            <?php foreach ($_SERVER["imagenes"] ?? [] as $imagen) : ?>
                                <tr>
                                    <th scope="row"><?= $imagen->getId() ?></th>
                                    <td>
                                        <img src="<?= $imagen->getURLPortfolio() ?>" alt="<?= $imagen->getNombre() ?>" title="<?= $imagen->getDescripcion() ?>" width="100px">
                                    </td>
                                    <td>
                                        <?= $imagen->getNombre() ?>
                                    </td>
                                    <td>
                                        <?= $imagen->getDescripcion() ?>
                                    </td>
                                    <td>
                                        <?= $imagen->getCategoria() ?>
                                    </td>

                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>

                    
                </form>
            </div>
        </div>
    </div>

    <?php 
        //Una función que hace requerir de la ruta absoluta para llamar al partial_loader
		require_once_absolute("views/partials/partial_loader.php");
	?>
</body>