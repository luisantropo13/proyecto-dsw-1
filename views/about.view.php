    <?php 
        require_once_absolute("views/partials/partial_head.php");
    ?> 
<body>

<div id="colorlib-page">
    <a href="#" class="js-colorlib-nav-toggle colorlib-nav-toggle"><i></i></a>
    <?php 
        require_once_absolute("views/partials/partial_aside.php");
    ?>
        <div id="colorlib-main">
            <section class="ftco-about img ftco-section ftco-no-pt ftco-no-pb" id="about-section">
                <div class="container-fluid px-0">
                    <div class="row d-flex">
                        <div class="col-md-6 d-flex">
                            <div class="img d-flex align-self-stretch align-items-center js-fullheight" style="background-image:url(images/about.jpg);">
                            </div>
                        </div>
                        <div class="col-md-6 d-flex align-items-center">
                            <div class="text px-4 pt-5 pt-md-0 px-md-4 pr-md-5 ftco-animate">
                                <h2 class="mb-4">Hola mi pana, soy yo <span>Luis</span> el dueño de este trabajo cochombroso</h2>
                                <p>Iba a poner más cosas en este proyecto, pero parece que la suerte no está de mi lado. Primero me tardó en llegar el ordenador casi un més, luego perdí el puente porque no me funcionaba la máquina virtual. En conclusión, soy un desgraciado</p>
                                <div class="team-wrap row mt-4">
                                    <div class="col-md-4 team">
                                        <div class="img" style="background-image: url(images/team-1.jpg);"></div>
                                        <h3>Repositorio</h3>
                                        <span>Bitbucket</span>
                                    </div>
                                    <div class="col-md-4 team">
                                        <div class="img" style="background-image: url(images/team-2.jpg);"></div>
                                        <h3>Instagram</h3>
                                        <span>Es coña</span>
                                    </div>
                                    <div class="col-md-4 team">
                                        <div class="img" style="background-image: url(images/team-3.jpg);"></div>
                                        <h3>Nuestra Web</h3>
                                        <span>Ya verás</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div><!-- END COLORLIB-MAIN -->
    </div><!-- END COLORLIB-PAGE -->

    <?php 
        require_once_absolute("views/partials/partial_loader.php");
    ?>
    
</body>
</html>