<?php 
    require_once "utils/utiles.php";
    require_once_absolute("utils/Menus.php");
    require_once_absolute("utils/Files.php");
    require_once_absolute("database/Coneccion.php");
    require_once_absolute("database/QueryBuilder.php");
    require_once_absolute("entity/ImagenGaleria.php");
    require_once_absolute("entity/Categoria.php");
    require_once_absolute("core/App.php");
    require_once_absolute("repository/ImagenGaleriaRepository.php");
    require_once_absolute("repository/CategoriaRepository.php");



    $errorres [] = ""; //ESTA AQUI
    $imagenes = array();
    $mensaje = "";
    
    try
    {
        $config = require_once("app/config.php");

        App::bind("configuracion", 1223);
        //App::get("logger")->add($mensaje);

        $imagenGaleriaRepository = new ImagenGaleriaRepository(); 
        $categoriaRepository = new CategoriaRepository(); 

        if ($_SERVER["REQUEST_METHOD"]==="POST") 
        {
            $categoria = trim(htmlspecialchars($_POST["categoria"]));

            $descripcion = trim(htmlspecialchars($_POST["descripcion"])) ;

            $tiposAceptados = ["image/jpeg", "image/png", "image/gif"];

            $imagen = new File("imagen", $tiposAceptados);       

            $imagen->saveUploadFile(ImagenGaleria::RUTA_IMAGENES_PORTAFOLIO);

            $imagenGaleria = new ImagenGaleria(0, $imagen->getFiles(), $descripcion, $categoria);

            $imagenGaleriaRepository->save($imagenGaleria);

            $_SERVER["mensaje"] = "Se ha guardado la imagen en la BBDD";

        }
        $_SERVER["imagenes"]= $imagenGaleriaRepository->findAll(); 
        $categorias = $categoriaRepository->findAll();  

    } catch (FileException $fileException) 
    {

        $errores [] = $fileException->getMessage();

    }
    catch (QueryException $queryException) 
    {

        $errores [] = $queryException->getMessage();

    }     
    catch (AppException $appException)
    {
        $errores [] = $appException->getMessage();
    } catch( Exception $e)
    {
      echo $e;

    }
   

    require_once_absolute("views/upload.view.php");

?>