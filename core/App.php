<?php 
    require_once_absolute("database/Coneccion.php");
    require_once_absolute("exceptions/AppException.php");
    class App{
        //Almacena los datos en nuestro contenedor
        public static $container = [];

        public static function bind (string $key, $value)
        {
            static::$container[$key] = $value;
        }

        public static function get(string $key)
        {
            if(!array_key_exists($key, static::$container))          
                throw new AppException("No se ha encontrado la clave $key en el contenedor.");
            return static::$container[$key];
        }

        public static function getConfigDB ( ) {
            return [
                "name" => "profotos",
                "username" => "userluis",
                "password" => "claveluis",
                "coneccion" => "mysql:host=localhost",
                "options" => [
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_PERSISTENT => true
                ]
            ];
        }

        public static function getConeccion()
        {
            if(!array_key_exists("coneccion", static::$container))
                static::$container["coneccion"] = coneccion::make( App::getConfigDB() );
            return static::$container["coneccion"];
        }
    }
?>