<?php
    require_once_absolute("exceptions/QueryException.php");
    require_once_absolute("exceptions/NotFoundException.php");
    require_once_absolute("core/App.php");
    require_once_absolute("database/IEntity.php");

    abstract class QueryBuilder implements IEntity{
        private $coneccion;
        private $table;
        private $classEntity;
        public function __construct(string $table, string $classEntity){ 
            $this->coneccion = App::getConeccion();
            $this->table = $table;
            $this->classEntity = $classEntity;
        }

        public function findAll(){
            $sql = "SELECT * FROM $this->table";
            $result = $this->executeQuery($sql);
        if(empty($result))
            throw new NotFoundException("No se ha encontrado el elemento con id");
            return $result; 
        }

        //Pendiente de explicar
        public function save(IEntity $entity): void
        {
            try
            {
                $parameters = $entity->toArray();

                $sql = sprintf("insert into %s (%s) values (%s)",

                    $this->table,

                    implode(", ", array_keys($parameters)),

                    ":".implode(", :", array_keys($parameters))
                );

                $statement = $this->coneccion->prepare($sql);
                $statement->execute($parameters);
            } 
            catch (PDOExceptions $exception)
            {
                throw new QueryException("Error al insertar en la BBDDS");
            }
        }

        public function executeQuery(string $sql): array
        {

            $pdoStatement = $this->coneccion->prepare($sql);

            if ($pdoStatement->execute()===false)

            throw new QueryException("No se ha podido ejecutar la consulta");

            return $pdoStatement->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $this->classEntity); 

        }

        public function find (int $id) : IEntity
        {
            $sql = "SELECT * FROM $this->table WHERE id=$id";
            $result = $this->executeQuery($sql);
            if (empty($result))
                throw new NotFoundException("No se ha encontrado el elemento con id $id");
            return $result[0]; 
        }

        public function toArray(): array
        {   
            return [];

        }
    }
?>