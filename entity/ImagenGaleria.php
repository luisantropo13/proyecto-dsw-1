<?php
require_once_absolute( "database/IEntity.php" );

class ImagenGaleria implements IEntity
{
    const RUTA_IMAGENES_PORTAFOLIO = "images/imgup/";

    private $id;
    private $nombre;
    private $descripcion;
    private $categoria;

    public function __construct( $id = 0, string $nombre ="", string $descripcion ="", $categoria = 0)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->descripcion = $descripcion;
        $this->categoria = $categoria;
    }

    public function getId() {return $this->id;}
    public function getNombre() { return $this->nombre; }
    public function getDescripcion() { return $this->descripcion; }
    public function getCategoria()   {return $this->categoria;}

    public function getURLPortfolio() : string
    {
        return  self::RUTA_IMAGENES_PORTAFOLIO."$this->nombre";
    }
    
    
    public function toArray(): array
    {
        return 
        [

            "id"=>$this->getId(),

            "nombre"=>$this->getNombre(),

            "descripcion"=>$this->getDescripcion(),

            "categoria"=>$this->getCategoria(),
        ];
    }
}
?>