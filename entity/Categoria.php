<?php
    require_once_absolute("database/QueryBuilder.php");
    class Categoria extends QueryBuilder
    {
        private $id;
        private $nombre;
        private $numimagen;
        private $descripcion;

        public function __construct($id = 0, $nombre ="", $descripcion ="")
        {
            $this->id = $id;
            $this->nombre = $nombre;
            $this->descripcion = $descripcion;
        }
        
        public function getId() {return $this->id;}
        public function getNombre() { return $this->nombre; }
        public function getDescripcion() { return $this->descripcion; }

        public function toArray(): array
    {
        return 
        [

            "id"=>$this->getId(),

            "nombre"=>$this->getNombre(),

            "descripcion"=>$this->getDescripcion()
        ];
    }
    }
?>