-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf16_spanish_ci DEFAULT NULL,
  `descripcion` text COLLATE utf16_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16 COLLATE=utf16_spanish_ci;

INSERT INTO `categoria` (`id`, `nombre`, `descripcion`) VALUES
(1,	'Fotos 1',	'Fotos de personas'),
(2,	'Fotos 2',	'Fotos de Comida'),
(3,	'Fotos 3',	'Fotos de Cosas');

DROP TABLE IF EXISTS `imagenes`;
CREATE TABLE `imagenes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) CHARACTER SET utf16 COLLATE utf16_spanish_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `categoria` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categoria` (`categoria`),
  CONSTRAINT `imagenes_ibfk_1` FOREIGN KEY (`categoria`) REFERENCES `categoria` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `imagenes` (`id`, `nombre`, `descripcion`, `categoria`) VALUES
(48,	'bulbasaur.png',	'AAAAAAAAAAAAAAAAAAAAAAAAAAAAA',	3),
(49,	'1607810800_bulbasaur.png',	'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA',	2),
(50,	'bidufgod.png',	'ME CAGO EN LA PUTAAAAAAAAAAAAAAAAAAAAAAAA',	3);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(25) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `users` (`id`, `usuario`, `email`, `clave`) VALUES
(1,	'aridany',	'qwerty@gmail.com',	'1234'),
(2,	'agustin',	'uiop@gmail.com',	'4567'),
(3,	'luis',	'zxcv@gmail.com',	'9874');

-- 2020-12-13 10:13:39
